import { IProduct } from './product.interface'
import IComponentOptions = angular.IComponentOptions;

export const SortModuleComponent: IComponentOptions = {

    bindings: {
        $router: '<',
    },

    controller: class {

        public options: String[];
        public products: IProduct[];
        public select: String;

        constructor(SortModule: any, public Products: any) {

            'ngInject';
            this.options = SortModule.options;
            this.products = Products.products;
            this.select = SortModule.options[0];
        };

        serviceSort(method: string, SortModule: any) {
            if (method == "promoted") {
                this.servicePromo();
                return;
            }
            this.products = this.Products.getProducts(method);
        }

        servicePromo() {

            this.products = this.Products.getPromoted();
        }

        indent(item: IProduct) {

            let spaces: any[] = [];
            for (let i = 0; i < 15 - item.name.length; i++) {
                spaces[i] = ' ';
            }
            return spaces;
        }
    },

    template: `
    <div>
      <h1>Sorted list component</h1>
      <select ng-model="$ctrl.select" ng-change="$ctrl.serviceSort($ctrl.select)">
        <option value='name'>Name</option>
        <option value='price'>Price</option>
        <option value='quantity'>Quantity</option>
        <option value='promoted'>Promoted</option>
      </select>
      <ul>

        <li ng-repeat="product in $ctrl.products" change-promoted-view product="product">
        <a ng-link="['Singleproduct', {id: product.id}]">{{ product.name }}</a><span ng-repeat="space in $ctrl.indent(product) track by $index">{{ space }}</span>{{ product.quantity }} x {{ product.price | pln }}
        </li>
      </ul>

    </div>
  `,
}
