import { IProduct } from './product.interface'
import IComponentOptions = angular.IComponentOptions;

export const SearchComponent: IComponentOptions = {

    controller: class {

        public products: IProduct[];
        public filter: String;

        constructor(Products: any) {

            'ngInject'
            this.products = Products.products;
            this.filter = '';
        };

        indent(item: IProduct) {

            let spaces: any[] = [];
            for (let i = 0; i < 15 - item.name.length; i++) {
                spaces[i] = ' ';
            }
            return spaces;
        }

        matchesProduct(filter: string, product: IProduct) {

            return product.name.toLowerCase().indexOf(filter.toLowerCase()) > -1;
        }
    },

    template: `
  <div>
    <h1>Search component</h1>
    <input ng-model="$ctrl.filter">
    <ul>
      <li ng-repeat="product in $ctrl.products" ng-show="$ctrl.matchesProduct($ctrl.filter, product)">
      {{ product.name }} <span ng-repeat="space in $ctrl.indent(product) track by $index">{{ space }}</span> {{ product.quantity }} x {{ product.price | currency }}
      </li>
    </ul>
  </div>
  `
}
