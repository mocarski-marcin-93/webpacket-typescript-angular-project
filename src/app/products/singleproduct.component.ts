import { IProduct } from './product.interface'
import IComponentOptions = angular.IComponentOptions;
import { IComponentOptionsRouter } from '../router/component-options-router.interface';
import '@angular/router/angular1/angular_1_router'

export const SingleProductComponent: IComponentOptions = {
    //
    // $routeConfig: [{
    //     component: 'singleproduct',
    //     name: 'Singleproduct',
    //     path: '/singleproduct/',
    //     useAsDefault: false,
    // }],

    bindings: {
        $router: '<',
    },

    controller: function(Products: any) {

        'ngInject';

        let _this = this;
        this.$routerOnActivate = function(next:any, previous: any) {
            let id = next.params.id;
            _this.product = Products.getProduct(id);
            console.log(_this.product);

        };



        // this.indent = function(item: IProduct): any {
        //
        //     let spaces: any[] = [];
        //     for (let i = 0; i < 15 - item.name.length; i++) {
        //         spaces[i] = ' ';
        //     }
        //     return spaces;
        // }
    },

    template: `
    <div>
      <h1>Single Product component</h1>
      <ul>
        <li>
          {{ $ctrl.product.name }} {{ $ctrl.product.quantity }} x {{ $ctrl.product.price | currency }}
        </li>
      </ul>
    </div>
  `,

}
