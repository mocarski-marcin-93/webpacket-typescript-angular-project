import IModule = angular.IModule
import { SingleProductComponent } from './singleproduct.component'
import { ProductsService } from './products.service'

//4/ Do modułu products przekazujemy teraz odpowiednie części
export const SingleProductModule: IModule = angular
  .module('app.component.singleproduct', [])
  .component('singleproduct', SingleProductComponent)
  .service('Products', ProductsService)
