import { IProduct } from './product.interface'
import IComponentOptions = angular.IComponentOptions;

export const ProductsComponent: IComponentOptions = {

  controller: class {

    public products: IProduct[];

    constructor(Products: any) {

      'ngInject';
      this.products = Products.products;
    }

    indent(item: IProduct) {

      let spaces:any[] = [];
        for(let i = 0; i < 15 - item.name.length; i++) {
            spaces[i] = ' ';
        }
        return spaces;
    }
  },

  template: `
    <div>
      <h1>Shopping list component</h1>
      <ul>
        <li ng-repeat="product in $ctrl.products">
          {{ product.name }} <span ng-repeat="space in $ctrl.indent(product) track by $index"> {{ space }}</span>{{ product.quantity }} x {{ product.price | currency }}
        </li>
      </ul>
    </div>
  `,
}
