import IModule = angular.IModule
import { SortModuleComponent } from './sortmodule.component'
import { SortModuleService } from './sortmodule.service'

//4/ Do modułu products przekazujemy teraz odpowiednie części
export const SortModuleModule: IModule = angular
  .module('app.component.sortmodule', [])
  .component('sortmodule', SortModuleComponent)
  .service('SortModule', SortModuleService)
