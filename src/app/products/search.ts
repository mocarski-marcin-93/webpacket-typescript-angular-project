import IModule = angular.IModule
import { SearchComponent } from './search.component'
import { ProductsService } from './products.service'

export const SearchModule:IModule = angular
  .module('app.component.search', [])
  .component('search', SearchComponent)
  .service('Products', ProductsService)
