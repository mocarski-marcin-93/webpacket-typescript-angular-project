import { IProduct } from './product.interface.ts'

export class ProductsService {

    public products: IProduct[] = [
        { id: 1, name: 'Milk', price: 1.05, quantity: 10, description: '', isPromo: false },
        { id: 2, name: 'Salami', price: 2.5, quantity: 20, description: '', isPromo: true },
        { id: 3, name: 'Bread', price: 0.85, quantity: 37, description: '', isPromo: false },
        { id: 4, name: 'Cheese', price: 5.5, quantity: 230, description: '', isPromo: true },
        { id: 5, name: 'Pork', price: 20.0, quantity: 9, description: '', isPromo: false },
        { id: 6, name: 'Cheese', price: 5.5, quantity: 230, description: '', isPromo: true },
        { id: 7, name: 'Cakes', price: 20.25, quantity: 500, description: '', isPromo: true },
        { id: 8, name: 'Computer', price: 2550, quantity: 1, description: '', isPromo: false },
        { id: 9, name: 'Chips', price: 2.0, quantity: 63, description: '', isPromo: true },
        { id: 10, name: 'Fish', price: 15.5, quantity: 57, description: '', isPromo: true },
    ]

    public getProduct(id: number): IProduct {
        let product: IProduct;
        for (let i = 0; i < this.products.length; i++) {
            if (id == this.products[i].id) {
                product = this.products[i];
            }
        }
        return product;
    };


    public getProducts(method: string): IProduct[] {
        let productsArray: IProduct[];
        let temp: IProduct[] = angular.copy(this.products);

        switch (method) {
            case 'name':
                productsArray = temp.sort(function(a, b) {
                    return a.name.localeCompare(b.name);
                });
                break;

            case 'price':
                productsArray = temp.sort(function(a, b) {
                    return a.price - b.price;
                });
                break;

            case 'quantity':
                productsArray = temp.sort(function(a, b) {
                    return a.quantity - b.quantity;
                });
                break;

            default:
                break;
        }
        return productsArray;
    }

    public getPromoted(): IProduct[] {

        let productsArray: IProduct[] = [];
        for (let i = 0; i < this.products.length; i++) {
            if (this.products[i].isPromo) {
                productsArray.push(this.products[i]);
            }
        }
        return productsArray;
    }
}
