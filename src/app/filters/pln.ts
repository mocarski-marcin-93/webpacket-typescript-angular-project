
export function plnFilterFactory() {
    return function plnFilter(input: number): string {
        const zl = Math.floor(input);
        const gr = Math.floor((input * 100) % 100);

        return `${zl}zl ${gr}gr`;
    };
}
