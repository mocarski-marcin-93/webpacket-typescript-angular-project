import IModule = angular.IModule
import IScope = angular.IScope
import IAttributes = angular.IAttributes;

import { ProductsModule } from './products/products'
import { SingleProductModule } from './products/singleproduct'
import { SortModuleModule } from './products/sortmodule'
import { SearchModule } from './products/search'
import { FormModule } from './form/form'

import { AppComponent } from './app.component'
import { IProduct } from './products/product.interface'

import { postalcodeDirective } from './form/postalcode.directive'

import { plnFilterFactory } from './filters/pln'

import '@angular/router/angular1/angular_1_router'


interface ProductScope extends IScope {
  product:IProduct;
}

function onOverflowChangePromotedView() {
    return {
      scope: {
        product: '<'
      },
      restrict: 'EA',
      link: function(scope:ProductScope, element:any, attr: IAttributes ) {
          angular.element(window).on('scroll', function(event){
            if(element[0].offsetTop > window.innerHeight - 100) {
              element.parent().addClass('posFixed');
              console.log(element[0].offsetTop + ' | ' + window.innerHeight);
            }
            else {
              element.removeClass('posFixed');
            }

          });
      }
    }
}

export const ComponentsModule: IModule = angular
  .module('app.components', [
    'ngComponentRouter',
    ProductsModule.name,
    SingleProductModule.name,
    SortModuleModule.name,
    SearchModule.name,
    FormModule.name
  ])
  .component('app', AppComponent)
  .directive('changePromotedView', onOverflowChangePromotedView)
  .directive('postalcode', postalcodeDirective)
  .filter('pln', plnFilterFactory)
  .value('$routerRootComponent', 'app')
  .config(($locationProvider:any) => {
    $locationProvider.html5Mode(true)
  });
