import IComponentOptions = angular.IComponentOptions
import { IComponentOptionsRouter } from './router/component-options-router.interface'


export const AppComponent: IComponentOptionsRouter = {

    $routeConfig: [{
        component: 'products',
        name: 'Products',
        path: 'products/',
        useAsDefault: false,
    }, {
      component: 'sortmodule',
      name: 'SortModule',
      path: 'sort/',
      useAsDefault: true,
    }, {
      component: 'singleproduct',
      name: 'Singleproduct',
      path: '/singleproduct/:id',
      useAsDefault: false,
    }],

    controller: class { },

    template: `<ng-outlet></ng-outlet>`,

}
