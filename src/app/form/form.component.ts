import { IProduct } from '../products/product.interface'
import { ProductsService } from '../products/products.service'
import IComponentOptions = angular.IComponentOptions;
import IFormController = angular.IFormController;

interface Order {

    product: IProduct;
    quantity: number;
    postalcode: string;
}



class FormComponentController {

    public products: IProduct[];
    public message: String;
    public valid: boolean;
    public order: Order;

    constructor(Products: any) {
        'ngInject';
        this.products = Products.products;
        this.message = '';
        this.valid = false;
    };

    public sendOrder(formController: IFormController) {
        if (formController.$valid) {
            this.message = "Form sent.";
        } else {
            this.message = "Something went terribly wrong! :(";
        }
    }

}
export const FormComponent: IComponentOptions = {
    controller: FormComponentController,
    template: `
    <div>
      <form name="orderForm" ng-submit="$ctrl.sendOrder(orderForm)" novalidate>

        <fieldset>
          <legend> Zamow coś </legend>

          <p class="formMessage" ng-if="$ctrl.message"> {{ $ctrl.message }} </p>

          <label for="chooseproduct"> Wybierz produkt: </label>
          <select name="chooseproduct" ng-model="$ctrl.order.product.name" required>
            <option ng-repeat="product in $ctrl.products" value="{{ product.name }}">{{ product.name }} </option>
          </select>

          <label for="insertquantity"> Podaj ilośc: </label>
          <input type="number" name="insertquantity" ng-model="$ctrl.order.quantity">

          <label for="insertpostalcode"> Podaj kod pocztowy: </label>
          <input type="text" name="insertpostalcode" ng-model="$ctrl.order.postalcode" postalcode>

          <input type="submit" value="Zrobione" ng-disabled="orderForm.$invalid">

        </fieldset>
      </form>

    </div>
  `
}
