import IAttributes = angular.IAttributes;
import IScope = angular.IScope;
import IngModelController = angular.INgModelController;

export function postalcodeDirective() {
    return {
        restrict: 'EA',
        require: 'ngModel',
        link($scope: IScope, $element: JQuery, $attrs: IAttributes, $ngModelCtrl: IngModelController) {
            $ngModelCtrl.$validators['postalcode'] = (modelValue: string, viewValue: string): boolean => {

                var regexp = /^\d{2}-\d{3}$/;
                return regexp.test(modelValue);
            }
        }
    }
}
