import IModule = angular.IModule
import { FormComponent } from './form.component'
import { ProductsService } from '..//products/products.service'

//4/ Do modułu products przekazujemy teraz odpowiednie części
export const FormModule: IModule = angular
  .module('app.component.form', [])
  .component('myform', FormComponent)
  .service('Products', ProductsService)
